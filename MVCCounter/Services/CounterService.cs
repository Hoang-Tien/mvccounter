﻿using MVCCounter.DataContext;
using MVCCounter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCCounter.Services
{
    public class CounterService
    {
        private readonly ICounterRepository _counterRepository;
        public CounterService(ICounterRepository counterRepository)
        {
            _counterRepository = counterRepository;
        }

        public bool Increase()
        {
            try
            {
                var max = _counterRepository.GetMaxNumber();
                if (max >= Constant.MAX_COUNT)
                {
                    return false;
                }

                Counter counter = new Counter
                {
                    Time = max + 1,
                    CreatedDate = DateTime.UtcNow
                };

                return _counterRepository.AddCounter(counter);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}