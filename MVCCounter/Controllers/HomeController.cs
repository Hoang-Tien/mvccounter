﻿using MVCCounter.DataContext;
using MVCCounter.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCCounter.Controllers
{
    public class HomeController : Controller
    {
        private ICounterRepository counterRepository;

        public HomeController()
        {
            this.counterRepository = new CounterRepository(new CounterContext());
        }

        public HomeController(ICounterRepository counterRepository)
        {
            this.counterRepository = counterRepository;
        }

        public ActionResult Index()
        {
            return View(this.counterRepository.GetMaxNumber());
        }
               
        public ActionResult Click()
        {
            CounterService counterService = new CounterService(this.counterRepository);
            counterService.Increase();
            return RedirectToAction("Index");
        }
    }    
}