﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCCounter.Models
{
    public class Counter
    {
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Time { get; set; }    
    }
}