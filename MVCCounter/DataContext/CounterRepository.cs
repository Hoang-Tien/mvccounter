﻿using MVCCounter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCCounter.DataContext
{
    public class CounterRepository : ICounterRepository, IDisposable
    {
        private CounterContext context;

        public CounterRepository(CounterContext context)
        {
            this.context = context;
        }

        public int GetMaxNumber()
        {
            try
            {
                if (!this.context.Counter.Any())
                {
                    return 0;
                }

                return this.context.Counter.Max(x => x.Time);
            }
            catch (Exception)
            {
                return 0;
            }

        }

        public bool AddCounter(Counter counter)
        {
            try
            {
                this.context.Counter.Add(counter);
                return SaveChanges() > 0;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public int SaveChanges()
        {
            return this.context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}