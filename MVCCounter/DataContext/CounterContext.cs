﻿using MVCCounter.Models;
using System.Data.Entity;

namespace MVCCounter.DataContext
{
    public class CounterContext : DbContext
    {
        public CounterContext()
            :base("name=DefaultConnection")
        {

        }

        public DbSet<Counter> Counter { get; set; }
    }
}