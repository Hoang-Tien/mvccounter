﻿using MVCCounter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCCounter.DataContext
{
    public interface ICounterRepository : IDisposable
    {
        int GetMaxNumber();
        bool AddCounter(Counter counter);
        int SaveChanges();
    }
}
