﻿using Moq;
using MVCCounter.DataContext;
using MVCCounter.Models;
using MVCCounter.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCCounter.Tests
{
    [TestFixture]
    public class CounterRepositoryTest
    {
        [Test]
        [TestCaseSource(typeof(CounterTestCaseExecuter), nameof(CounterTestCaseExecuter.TC_TSController_IncreaseFunc))]
        public void TestIncreaseOverMaxCount(CounterTestCaseData data)
        {
            int max = int.Parse(data.InputData.ToString()); 
            var icounterRepositoryMock = new Mock<ICounterRepository>();
            icounterRepositoryMock.Setup(x => x.AddCounter(It.IsAny<Counter>())).Returns(true);
            icounterRepositoryMock.Setup(x => x.GetMaxNumber()).Returns(max);
            CounterService counterService = new CounterService(icounterRepositoryMock.Object);
            bool result = counterService.Increase();
            Assert.AreEqual(data.ExpectedData, result);
        }
    }
}
