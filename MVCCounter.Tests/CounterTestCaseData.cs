﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCCounter.Tests
{
    public class CounterTestCaseData
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public object InputData { get; set; }
        public object ExpectedData { get; set; }
    }
}
