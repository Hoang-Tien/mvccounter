﻿using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCCounter.Tests
{
    public static class CounterTestCaseExecuter
    {
        public static IEnumerable TC_TSController_IncreaseFunc
        {
            get
            {
                string location = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Data\TCService.json"); ;
                List<CounterTestCaseData> listData = ReadData(location);
                return ExecuteTest(listData);
            }
        }

        private static IEnumerable ExecuteTest(List<CounterTestCaseData> listData)
        {
            foreach (var item in listData)
            {
                TestCaseData data = new TestCaseData(item);
                data.SetName(item.Name);
                data.SetDescription(item.Description);
                yield return data;
            }
        }

        /// <summary>
        /// Read data aand seriable follow specific object
        /// </summary>
        /// <param name="filePath">file path</param>
        /// <returns></returns>
        private static List<CounterTestCaseData> ReadData(string filePath)
        {
            try
            {
                List<CounterTestCaseData> list = null;
                using (StreamReader file = File.OpenText(filePath))
                using (JsonTextReader reader = new JsonTextReader(file))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    list = serializer.Deserialize<List<CounterTestCaseData>>(reader);
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
